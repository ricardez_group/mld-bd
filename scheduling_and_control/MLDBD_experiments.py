from __future__ import division
from pickle import TRUE
import sys
sys.path.append('C:/Users/dlinanro/Desktop/mld-bd/') 
from functions.d_bd_functions import run_function_dbd
from functions.dsda_functions import get_external_information,external_ref,solve_subproblem,solve_subproblem_aprox_fix_all_scheduling,generate_initialization,initialize_model,solve_with_minlp,sequential_iterative_2_case2,sequential_non_iterative_2_case2,sequential_non_iterative_2
import pyomo.environ as pe
from pyomo.gdp import Disjunct, Disjunction
import math
from pyomo.opt.base.solvers import SolverFactory
import io
import time
from functions.dsda_functions import neighborhood_k_eq_all,neighborhood_k_eq_l_natural,neighborhood_k_eq_2,get_external_information,external_ref,solve_subproblem,generate_initialization,initialize_model,solve_with_dsda,sequential_iterative_2
from functions.d_bd_functions import run_function_dbd_aprox
import logging
from case_study_2_model import problem_logic_scheduling,problem_logic_scheduling_complete,case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential,case_2_scheduling_control_gdp_var_proc_time_min_proc_time,case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation, case_2_scheduling_control_gdp_var_proc_time_min_proc_time_with_distillation,case_2_scheduling_only_lower_bound_tau  
import os
import matplotlib.pyplot as plt
from case_study_1_model import scheduling_and_control_gdp_N_solvegdp_simpler,scheduling_and_control_gdp_N_approx_sequential_naive,problem_logic_scheduling as problem_logic_scheduling_case1
from case_study_1_model import scheduling_only_gdp_N_solvegdp_simpler,scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau
from case_study_1_model import scheduling_and_control_gdp_N_approx_sequential
from case_study_1_model import  problem_logic_scheduling_dummy
import numpy as np
from math import fabs
if __name__ == "__main__":
    #Do not show warnings
    logging.getLogger('pyomo').setLevel(logging.ERROR)
    #Experiments to run: case study 1 (short scheduling horizon)
    case_1_sequential_naive=False
    case_1_sequential_iterative=False
    case_1_LD_BD_naive=False
    case_1_MLD_BD_naive=False
    case_1_MLD_BD_pruning_rig_subproblems=False
    case_1_MLD_BD_pruning_aprox_subproblems=False
    case_1_MLD_BD_pruning_aprox_subproblems_from_infeasible=False
    case_1_dicopt=False
    #Experiments to run: case study 1 (long scheduling horizon)
    case_1_sequential_naive_long_horizon=False
    case_1_sequential_iterative_long_horizon=False
    case_1_MLD_BD_naive_long_horizon=False
    case_1_MLD_BD_pruning_rig_subproblems_long_horizon=False
    case_1_MLD_BD_pruning_aprox_subproblems_long_horizon=False
    case_1_MLD_BD_pruning_aprox_subproblems_long_horizon_from_infeasible=False
    case_1_dicopt_long_horizon=False
    #Experiments to run: case study 2 (without distillation dynamics)
    case_2_sequential_naive=False
    case_2_sequential_iterative=False
    case_2_MLD_BD_pruning_aprox_subproblems=False
    #Experiments to run: case study 2 (with distillation dynamics)
    case_2_sequential_naive_with_distillation=False
    case_2_sequential_iterative_with_distillation=False
    case_2_dicopt_with_distillation=False
    case_2_MLD_BD_pruning_aprox_subproblems_with_distillation=False
    case_2_MLD_BD_pruning_aprox_subproblems_from_infeasible_with_distillation=False


####CASE STUDY 1###############################

    print('******CASE STUDY 1: CHU AND YOU, SHORT SCHEDULING HORIZON (14 h)************')


# ###############################################################################
# #########--------------base case ------------------############################
# ###############################################################################
# ###############################################################################

    initialization=[1, 1, 1, 1, 1, 1]
  
    mip_solver='CPLEX'
    minlp_solver='DICOPT'
    nlp_solver='conopt4'
    transform='bigm'


    if minlp_solver=='dicopt' or minlp_solver=='DICOPT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','stop 3 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
        print('DICOPT options:',sub_options)
    elif minlp_solver=='OCTERACT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','Option Threads =0;','Option SOLVER = OCTERACT;','$onecho > octeract.opt \n','LOCAL_SEARCH true\n','$offecho \n']}
    
    kwargs={}


# ###############################################################################
# #########--------------sequential naive-------------###########################
# ###############################################################################
# ###############################################################################
    if case_1_sequential_naive:
        print('\n-------SEQUENTIAL NAIVE-------------------------------------')
        kwargs2=kwargs.copy()
        kwargs2['sequential']=True

        logic_fun=problem_logic_scheduling_case1
        model_fun=scheduling_and_control_gdp_N_approx_sequential_naive
        m=model_fun(**kwargs2)

        for c in m.component_objects(ctype=pe.Param, descend_into=True):
            c.pprint()
        for c in m.component_objects(ctype=pe.RangeSet, descend_into=True):
            c.pprint()

        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors if m.I_i_j_prod[I,J]==1}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        initialization_test=[]
        for k in upper_bounds.keys():
            initialization_test.append(upper_bounds[k]) 
        print('upper bound ext var related to proc time',initialization_test)
        ## RUN THIS TO SOLVE
        m=sequential_non_iterative_2(logic_fun,initialization_test,model_fun,kwargs2,ext_ref,provide_starting_initialization= False, subproblem_solver=nlp_solver,subproblem_solver_options=sub_options,tee = False, global_tee= True,rel_tol = 0)
        #SAVE SOLUTION
        save=generate_initialization(m=m,model_name='case_1_scheduling_and_dynamics_solution')
        ## RUN THIS TO RETRIEVE SOLUTION    

        m=initialize_model(m,from_feasible=True,feasible_model='case_1_scheduling_and_dynamics_solution')
        # m.varTime.pprint()

        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))

###############################################################################
#########--------------sequential iterative-------------#######################
###########------------------------------------------##########################
###############################################################################
    if case_1_sequential_iterative:
        print('\n-------SEQUENTIAL ITERATIVE-------------------------------------')
        # STEP 1: SCHEDULING ONLY WITH VARIABLE PROCESSSING TIMES
        kwargs3=kwargs.copy()
        kwargs3['x_initial']=[1,1,1,1,1,1]
        model_fun=scheduling_only_gdp_N_solvegdp_simpler
        m=model_fun(**kwargs)
        m = solve_with_minlp(m,transformation=transform,minlp=mip_solver,minlp_options=sub_options,timelimit=3600000,gams_output=False,tee=True,rel_tol=0)
        save=generate_initialization(m=m,model_name='case_1_scheduling_only_solution')
        # SCHEDULING INITIALIZATION
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)

        # STEP 2: SEQUENTIAL STRATEGY TO IDENTIFY FEASIBILITY
        model_fun =scheduling_and_control_gdp_N_approx_sequential
        logic_fun=problem_logic_scheduling_dummy
        m=model_fun()
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)
        m,Sol_found=sequential_iterative_2(logic_fun,Sol_found,model_fun,kwargs,ext_ref,rate_tau=1,provide_starting_initialization = False,subproblem_solver=nlp_solver,iter_timelimit = 1000000,subproblem_solver_options=sub_options,gams_output = False,tee = False,global_tee = True,rel_tol = 0)
        

        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))


        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        logic_fun=problem_logic_scheduling_case1
        m_partial=model_fun(**kwargs)
        def _obj_scheduling(m_partial):
            return ( m_partial.TCP1+m_partial.TCP2+m_partial.TMC-m_partial.SALES  )/100
        m_partial.obj_scheduling = pe.Objective(rule=_obj_scheduling, sense=pe.minimize)  
        
        def _obj_dummy(m_partial):
            return 1
        m_partial.obj_dummy = pe.Objective(rule=_obj_dummy, sense=pe.minimize)  

        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        # ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=False)
        m_partial = external_ref(m=m_partial,x=Sol_found,extra_logic_function=logic_fun,dict_extvar=reformulation_dict,mip_ref=False,tee=False)

        
        m_partial=solve_subproblem_aprox_fix_all_scheduling(m_partial,subproblem_solver=nlp_solver,subproblem_solver_options = sub_options,timelimit = 86400, gams_output = False,tee = True,rel_tol = 0)   
        save=generate_initialization(m=m_partial,model_name='case_1_scheduling_and_dynamics_solution_seq_iterative')


        Sol_found_seq_naive=Sol_found

        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))

###############################################################################
#########--------------LD-BD METHODOLOGIES--------------#######################
###########------------FROM FEASIBLE      -----------##########################
##############################################################################
    # NOTE: IN CASE I DO NOT WANT TO RUN PREVIOUS CODE
    if not case_1_sequential_naive:
        Sol_found_seq_naive=[4,4,5,5,3,3,3,2,2,3,3,2,2,2,3,2]
    if case_1_LD_BD_naive:
        print('\n-------LDBD NAIVE-------------------------------------')
        initialization=Sol_found_seq_naive 
        infinity_val=1e+4
        maxiter=1000
        neigh=neighborhood_k_eq_2(len(initialization))
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        logic_fun=problem_logic_scheduling_case1
        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd(initialization,infinity_val,minlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,one_cut_per_iteration=True)
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))
        save=generate_initialization(m=m,model_name='case_1_scheduling_and_dynamics_solution_DBD_one_cut_naive')
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))

    if case_1_MLD_BD_naive:
        print('\n-------MULTICUT LDBD NAIVE-------------------------------------')
        initialization=Sol_found_seq_naive 
        infinity_val=1e+4
        maxiter=1000
        neigh=neighborhood_k_eq_2(len(initialization))
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        logic_fun=problem_logic_scheduling_case1
        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd(initialization,infinity_val,minlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True)
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))
        save=generate_initialization(m=m,model_name='case_1_scheduling_and_dynamics_solution_DBD_multicut_naive')
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))

    if case_1_MLD_BD_pruning_rig_subproblems:
        print('\n-------MULTICUT LDBD PRUNING RIGUROUS SUBPROBLEMS-------------------------------------')
        Sol_found=Sol_found_seq_naive # from sequential iterative
        feas_model='case_1_scheduling_and_dynamics_solution' # from sequential iterative
        initialization=Sol_found
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'
        neigh=neighborhood_k_eq_2(len(Sol_found))
        logic_fun=problem_logic_scheduling_case1
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        model_fun_scheduling=scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau
        kwargs['prunning']=True
        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()   
        # NOTE: go to dsda_functions->solve_subproblem_aprox-> change approximate_solution as needed                                                                                                                                                            ## TODO: this second model function is a version of the model with only scheduling constraints. Work on this!!!!!
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,minlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun_scheduling,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=0,new_case=False,with_distillation=False,provide_starting_initialization=True,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_1_scheduling_and_dynamics_solution_DBD_pruning_rigurous_subpr_'+minlp_solver+'_'+neighdef
        save=generate_initialization(m=m,model_name=solname) 
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))
        kwargs['prunning']=False

    if case_1_MLD_BD_pruning_aprox_subproblems:
        print('\n-------MULTICUT LDBD PRUNING APROX SUBPROBLEMS-------------------------------------')
        Sol_found=Sol_found_seq_naive # from sequential iterative
        feas_model='case_1_scheduling_and_dynamics_solution' # from sequential iterative
        kwargs['prunning']=True
        initialization=Sol_found
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'
        neigh=neighborhood_k_eq_2(len(Sol_found))
        logic_fun=problem_logic_scheduling_case1
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        model_fun_scheduling=scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau

        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()
        # NOTE: go to dsda_functions->solve_subproblem_aprox-> change approximate_solution as needed                                                                                                                                                              ## TODO: this second model function is a version of the model with only scheduling constraints. Work on this!!!!!
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,nlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun_scheduling,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=0,new_case=False,with_distillation=False,provide_starting_initialization=True,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_1_scheduling_and_dynamics_solution_DBD_pruning_aprox_subpr_'+minlp_solver+'_'+neighdef+'test'
        save=generate_initialization(m=m,model_name=solname) 
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))
        kwargs['prunning']=False
        # print(sub_options)

###############################################################################
#########--------------LD-BD METHODOLOGIES--------------#######################
###########------------FROM INFEASIBLE  ----------   -----------###############
###############################################################################
    if case_1_MLD_BD_pruning_aprox_subproblems_from_infeasible:
        print('\n-------MULTICUT LDBD PRUNING APROX SUBPROBLEMS FROM INFEASIBLE-------------------------------------')

        feas_model='' 
        kwargs['prunning']=True
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'

        logic_fun=problem_logic_scheduling_case1
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        model_fun_scheduling=scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau

        m_init=model_fun(**kwargs)

        m_init=initialize_model(m_init,from_feasible=True,feasible_model='case_1_scheduling_only_solution')
        Sol_found=[]
        for I in m_init.I_reactions:
            for J in m_init.J_reactors:
                if m_init.I_i_j_prod[I,J]==1:
                    for K in m_init.ordered_set[I,J]:
                        if round(pe.value(m_init.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m_init.minTau[I,J]+1)
        for I_J in m_init.I_J:
            Sol_found.append(1+round(pe.value(m_init.Nref[I_J])))
        print('EXT_VARS_INFEASIBLE_INIT',Sol_found)


        neigh=neighborhood_k_eq_2(len(Sol_found))
        initialization=Sol_found

        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()                                                                                                                                                              ## TODO: this second model function is a version of the model with only scheduling constraints. Work on this!!!!!
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,nlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun_scheduling,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=0,new_case=False,with_distillation=False,provide_starting_initialization=False,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_1_scheduling_and_dynamics_solution_DBD_pruning_aprox_subpr_from_infeasible_'+minlp_solver+'_'+neighdef
        save=generate_initialization(m=m,model_name=solname) 
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))
        kwargs['prunning']=False

# ###############################################################################
# #########--------------dicopt ----------------#################################
# ###############################################################################
# ###############################################################################
    if case_1_dicopt:
        stdoutOrigin=sys.stdout
        dir_path = os.path.dirname(os.path.abspath(__file__))
        file_name=dir_path+'/'+'dicopt_case_1'
        sys.stdout = open(file_name+".txt", "w")

        print('\n-------DICOPT-------------------------------------',flush=True)
        init_name='case_1_scheduling_and_dynamics_solution'

        model_fun=scheduling_and_control_gdp_N_solvegdp_simpler
        m=model_fun(**kwargs)
        if minlp_solver=='dicopt' or minlp_solver=='DICOPT':
            # NOTE: we have to modify options slighlty to guarantee that DICOPT starts from user provided initialization!!!!!!! If we remove this, DICOPT WILL NEVER FIND A FEASIBLE SOLUTION!!!
            # sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','stop 3 \n','relaxed 0 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
            sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}


        m=initialize_model(m=m,from_feasible=True,feasible_model=init_name) 
        start=time.time()
        m=solve_with_minlp(m,transformation=transform,minlp=minlp_solver,minlp_options=sub_options,timelimit=86400,gams_output=False,tee=True,rel_tol=0)
        end=time.time()    
        solname='case_1_minlp_'+minlp_solver+'_from_'+init_name
        save=generate_initialization(m=m,model_name=solname)

        if m.results.solver.termination_condition == 'infeasible' or m.results.solver.termination_condition == 'other' or m.results.solver.termination_condition == 'unbounded' or m.results.solver.termination_condition == 'invalidProblem' or m.results.solver.termination_condition == 'solverFailure' or m.results.solver.termination_condition == 'internalSolverError' or m.results.solver.termination_condition == 'error'  or m.results.solver.termination_condition == 'resourceInterrupt' or m.results.solver.termination_condition == 'licensingProblem' or m.results.solver.termination_condition == 'noSolution' or m.results.solver.termination_condition == 'noSolution' or m.results.solver.termination_condition == 'intermediateNonInteger': 
            m.dicopt_status='Infeasible'
        else:
            m.dicopt_status='Optimal'

        if m.dicopt_status=='Optimal':
            Sol_founddicopt=[]
            for I in m.I_reactions:
                for J in m.J_reactors:
                    if m.I_i_j_prod[I,J]==1:
                        for K in m.ordered_set[I,J]:
                            if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                                Sol_founddicopt.append(K-m.minTau[I,J]+1)
            for I_J in m.I_J:
                Sol_founddicopt.append(1+round(pe.value(m.Nref[I_J])))


            print('Objective DICOPT=',pe.value(m.obj),'best DICOPT=',Sol_founddicopt,'cputime DICOPT=',str(end-start),flush=True)
        else:
            print('DICOPT infeasible','cputime DICOPT=',str(end-start),flush=True)

        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJVAL=(TPC1+TPC2+TPC3+TMC-SALES)
        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1),flush=True)   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2),flush=True)
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3),flush=True)
        print('TMC: Total material cost: ',str(TMC),flush=True)
        print('SALES: Revenue form selling products: ',str(SALES),flush=True)
        print('OBJ:',str(OBJVAL),flush=True)

        sys.stdout.close()
        sys.stdout=stdoutOrigin
####CASE STUDY 1.1###############################

    print('******CASE STUDY 1: CHU AND YOU, LONG SCHEDULING HORIZON (28 h)************')

# ###############################################################################
# #########--------------base case ------------------############################
# ###############################################################################
# ###############################################################################

    initialization=[1, 1, 1, 1, 1, 1]
  
    mip_solver='CPLEX'
    minlp_solver='DICOPT'
    nlp_solver='conopt4'
    transform='bigm'


    if minlp_solver=='dicopt' or minlp_solver=='DICOPT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','stop 3 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
        print('DICOPT options:',sub_options)
    elif minlp_solver=='OCTERACT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','Option Threads =0;','Option SOLVER = OCTERACT;','$onecho > octeract.opt \n','LOCAL_SEARCH true\n','$offecho \n']}
    
    kwargs={'last_time_hours':28,'demand_p1_kmol':2,'demand_p2_kmol':2}
    rel_tol=0.05


# ###############################################################################
# #########--------------sequential naive-------------###########################
# ###############################################################################
# ###############################################################################
    if case_1_sequential_naive_long_horizon:
        print('\n-------SEQUENTIAL NAIVE-------------------------------------')
        kwargs2=kwargs.copy()
        kwargs2['sequential']=True

        logic_fun=problem_logic_scheduling_case1
        model_fun=scheduling_and_control_gdp_N_approx_sequential_naive
        m=model_fun(**kwargs2)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors if m.I_i_j_prod[I,J]==1}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        initialization_test=[]
        for k in upper_bounds.keys():
            initialization_test.append(upper_bounds[k]) 
        print('upper bound ext var related to proc time',initialization_test)
        ## RUN THIS TO SOLVE
        m=sequential_non_iterative_2(logic_fun,initialization_test,model_fun,kwargs2,ext_ref,provide_starting_initialization= False, subproblem_solver=nlp_solver,subproblem_solver_options=sub_options,tee = False, global_tee= True,rel_tol = rel_tol)
        #SAVE SOLUTION
        save=generate_initialization(m=m,model_name='case_1_28h_scheduling_and_dynamics_solution')
        ## RUN THIS TO RETRIEVE SOLUTION    

        m=initialize_model(m,from_feasible=True,feasible_model='case_1_28h_scheduling_and_dynamics_solution')
        # m.varTime.pprint()

        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))

###############################################################################
#########--------------sequential iterative-------------#######################
###########-------------- ---------------------------##########################
###############################################################################
    if case_1_sequential_iterative_long_horizon:
        print('\n-------SEQUENTIAL ITERATIVE-------------------------------------')
        # STEP 1: SCHEDULING ONLY WITH VARIABLE PROCESSSING TIMES
        kwargs3=kwargs.copy()
        kwargs3['x_initial']=[1,1,1,1,1,1]
        model_fun=scheduling_only_gdp_N_solvegdp_simpler
        m=model_fun(**kwargs)
        m = solve_with_minlp(m,transformation=transform,minlp=mip_solver,minlp_options=sub_options,timelimit=3600000,gams_output=False,tee=True,rel_tol=rel_tol)
        save=generate_initialization(m=m,model_name='case_1_28h_scheduling_only_solution')
        # SCHEDULING INITIALIZATION
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)

        # STEP 2: SEQUENTIAL STRATEGY TO IDENTIFY FEASIBILITY
        model_fun =scheduling_and_control_gdp_N_approx_sequential
        logic_fun=problem_logic_scheduling_dummy
        m=model_fun()
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)
        m,Sol_found=sequential_iterative_2(logic_fun,Sol_found,model_fun,kwargs,ext_ref,rate_tau=1,provide_starting_initialization = False,subproblem_solver=nlp_solver,iter_timelimit = 1000000,subproblem_solver_options=sub_options,gams_output = False,tee = False,global_tee = True,rel_tol = rel_tol)
        save=generate_initialization(m=m,model_name='case_1_28h_scheduling_and_dynamics_solution_seq_iterative')

        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))


        Sol_found_seq_naive=Sol_found
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))



###############################################################################
#########--------------LD-BD METHODOLOGIES--------------#######################
###########------------FROM FEASIBLE      -----------##########################
###############################################################################
    if case_1_MLD_BD_naive_long_horizon:
        print('\n-------MULTICUT LDBD NAIVE-------------------------------------')
        # #NOTE: IN CASE I DO NOT WANT TO RUN PREVIOUS CODE: Sol_found_seq_naive=
        # Sol_found_seq_naive=
        initialization=Sol_found_seq_naive 
        infinity_val=1e+4
        maxiter=1000
        neigh=neighborhood_k_eq_2(len(initialization))
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        logic_fun=problem_logic_scheduling_case1
        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd(initialization,infinity_val,minlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True)
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))
        save=generate_initialization(m=m,model_name='case_1_28h_scheduling_and_dynamics_solution_DBD_multicut_naive')
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))

    if case_1_MLD_BD_pruning_rig_subproblems_long_horizon:
        print('\n-------MULTICUT LDBD PRUNING RIGUROUS SUBPROBLEMS-------------------------------------')
        # #NOTE: IN CASE I DO NOT WANT TO RUN PREVIOUS CODE: Sol_found_seq_naive=
        # Sol_found_seq_naive=
        Sol_found=Sol_found_seq_naive # from sequential iterative
        feas_model='case_1_28h_scheduling_and_dynamics_solution' # from sequential iterative
        initialization=Sol_found
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'
        neigh=neighborhood_k_eq_2(len(Sol_found))
        logic_fun=problem_logic_scheduling_case1
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        model_fun_scheduling=scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau
        kwargs['prunning']=True
        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()  
        # NOTE: go to dsda_functions->solve_subproblem_aprox-> change approximate_solution as needed                                                                                                                                                             ## TODO: this second model function is a version of the model with only scheduling constraints. Work on this!!!!!
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,minlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun_scheduling,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=0,new_case=False,with_distillation=False,provide_starting_initialization=True,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_1_28h_scheduling_and_dynamics_solution_DBD_pruning_rigurous_subpr_'+minlp_solver+'_'+neighdef
        save=generate_initialization(m=m,model_name=solname) 
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))
        kwargs['prunning']=False

    if case_1_MLD_BD_pruning_aprox_subproblems_long_horizon:
        print('\n-------MULTICUT LDBD PRUNING APROX SUBPROBLEMS-------------------------------------')
        # #NOTE: IN CASE I DO NOT WANT TO RUN PREVIOUS CODE: Sol_found_seq_naive=
        # Sol_found_seq_naive=[4, 4, 5, 5, 3, 3, 4, 3, 3, 5, 5, 5, 4, 6, 5, 7] #NOTE: this is sequential naive!!!!!--still have to run seq iterative!!!!
        Sol_found=Sol_found_seq_naive # from sequential iterative
        feas_model='case_1_28h_scheduling_and_dynamics_solution' # from sequential iterative
        kwargs['prunning']=True
        initialization=Sol_found
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'
        neigh=neighborhood_k_eq_2(len(Sol_found))
        logic_fun=problem_logic_scheduling_case1
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        model_fun_scheduling=scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau

        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()    
        # NOTE: go to dsda_functions->solve_subproblem_aprox-> change approximate_solution as needed                                                                                                                                                           ## TODO: this second model function is a version of the model with only scheduling constraints. Work on this!!!!!
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,nlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun_scheduling,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=rel_tol,new_case=False,with_distillation=False,provide_starting_initialization=True,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_1_28h_scheduling_and_dynamics_solution_DBD_pruning_aprox_subpr_'+minlp_solver+'_'+neighdef+'test'
        save=generate_initialization(m=m,model_name=solname) 
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))
        kwargs['prunning']=False

###############################################################################
#########--------------LD-BD METHODOLOGIES--------------#######################
###########------------FROM INFEASIBLE  ---------    -----------###############
###############################################################################
    if case_1_MLD_BD_pruning_aprox_subproblems_long_horizon_from_infeasible:
        print('\n-------MULTICUT LDBD PRUNING APROX SUBPROBLEMS FROM INFEASIBLE-------------------------------------')

        feas_model='' 
        kwargs['prunning']=True

        infinity_val=1e+4
        maxiter=10000
        neighdef='2'

        logic_fun=problem_logic_scheduling_case1
        model_fun =scheduling_and_control_gdp_N_solvegdp_simpler
        model_fun_scheduling=scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau

        m_init=model_fun(**kwargs)

        m_init=initialize_model(m_init,from_feasible=True,feasible_model='case_1_28h_scheduling_only_solution')
        Sol_found=[]
        for I in m_init.I_reactions:
            for J in m_init.J_reactors:
                if m_init.I_i_j_prod[I,J]==1:
                    for K in m_init.ordered_set[I,J]:
                        if round(pe.value(m_init.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m_init.minTau[I,J]+1)
        for I_J in m_init.I_J:
            Sol_found.append(1+round(pe.value(m_init.Nref[I_J])))
        print('EXT_VARS_INFEASIBLE_INIT',Sol_found)


        neigh=neighborhood_k_eq_2(len(Sol_found))
        initialization=Sol_found

        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I_reactions for J in m.J_reactors}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()  
        # NOTE: go to dsda_functions->solve_subproblem_aprox-> change approximate_solution as needed                                                                                                                                                             ## TODO: this second model function is a version of the model with only scheduling constraints. Work on this!!!!!
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,nlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun_scheduling,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=rel_tol,new_case=False,with_distillation=False,provide_starting_initialization=False,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_1_28h_scheduling_and_dynamics_solution_DBD_pruning_aprox_subpr_from_infeasible_'+minlp_solver+'_'+neighdef
        save=generate_initialization(m=m,model_name=solname) 
        Sol_found=[]
        for I in m.I_reactions:
            for J in m.J_reactors:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print('EXT_VARS_FOUND',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_FOUND=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE:',str(OBJ_FOUND))
        kwargs['prunning']=False


###############################################################################
#########--------------dicopt ----------------#################################
###############################################################################
###############################################################################
    if case_1_dicopt_long_horizon:
        print('\n-------DICOPT-------------------------------------')
        model_fun=scheduling_and_control_gdp_N_solvegdp_simpler
        m=model_fun(**kwargs)
        if minlp_solver=='dicopt' or minlp_solver=='DICOPT':
            # NOTE: we have to modify options slighlty to guarantee that DICOPT starts from user provided initialization!!!!!!! If we remove this, DICOPT WILL NEVER FIND A FEASIBLE SOLUTION!!!
            sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','stop 3 \n','relaxed 0 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
        m=initialize_model(m,from_feasible=True,feasible_model='case_1_28h_scheduling_and_dynamics_solution') 

        start=time.time()
        m=solve_with_minlp(m,transformation=transform,minlp=minlp_solver,minlp_options=sub_options,timelimit=100000,gams_output=False,tee=True,rel_tol=0)
        end=time.time()    
        solname='case_1_28h_minlp_'+minlp_solver
        save=generate_initialization(m=m,model_name=solname)

        if m.results.solver.termination_condition == 'infeasible' or m.results.solver.termination_condition == 'other' or m.results.solver.termination_condition == 'unbounded' or m.results.solver.termination_condition == 'invalidProblem' or m.results.solver.termination_condition == 'solverFailure' or m.results.solver.termination_condition == 'internalSolverError' or m.results.solver.termination_condition == 'error'  or m.results.solver.termination_condition == 'resourceInterrupt' or m.results.solver.termination_condition == 'licensingProblem' or m.results.solver.termination_condition == 'noSolution' or m.results.solver.termination_condition == 'noSolution' or m.results.solver.termination_condition == 'intermediateNonInteger': 
            m.dicopt_status='Infeasible'
        else:
            m.dicopt_status='Optimal'

        if m.dicopt_status=='Optimal':
            Sol_founddicopt=[]
            for I in m.I_reactions:
                for J in m.J_reactors:
                    if m.I_i_j_prod[I,J]==1:
                        for K in m.ordered_set[I,J]:
                            if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                                Sol_founddicopt.append(K-m.minTau[I,J]+1)
            for I_J in m.I_J:
                Sol_founddicopt.append(1+round(pe.value(m.Nref[I_J])))


            print('Objective DICOPT=',pe.value(m.obj),'best DICOPT=',Sol_founddicopt,'cputime DICOPT=',str(end-start))
        else:
            print('DICOPT infeasible','cputime DICOPT=',str(end-start))

        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJVAL=(TPC1+TPC2+TPC3+TMC-SALES)
        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJ:',str(OBJVAL))

# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #



####CASE STUDY 2###############################

    print('******CASE STUDY 2: SEMIBATCH, without distillation column************')

# ###############################################################################
# #########--------------base case ------------------############################
# ###############################################################################
# ###############################################################################

    obj_Selected='profit_max'



    initialization=[1, 1, 1, 1, 1, 1, 1, 1]
  
    mip_solver='CPLEX'
    minlp_solver='DICOPT'
    nlp_solver='conopt4'
    transform='bigm'
    #tried 5 and no improvement. With 15 DICOT is unable, and now DSDA can solve the problem.
    last_disc=15
    last_time_h=5

    if minlp_solver=='dicopt' or minlp_solver=='DICOPT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
        print('DICOPT options:',sub_options)
    elif minlp_solver=='OCTERACT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','Option Threads =0;','Option SOLVER = OCTERACT;','$onecho > octeract.opt \n','LOCAL_SEARCH true\n','$offecho \n']}
    
    LO_PROC_TIME={('T1','U1'):0.5,('T2','U2'):0.1,('T2','U3'):0.1,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):1.5}
    UP_PROC_TIME={('T1','U1'):0.5,('T2','U2'):2,('T2','U3'):2,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):1.5}
    kwargs={'obj_type':obj_Selected,'last_disc_point':last_disc,'last_time_hours':last_time_h,'lower_t_h':LO_PROC_TIME,'upper_t_h':UP_PROC_TIME,'sequential':False}




# ###############################################################################
# #########--------------sequential naive-------------###########################
# ###############################################################################
# ###############################################################################
    if case_2_sequential_naive:
        initialization_test=[1, 6, 6, 1, 1, 1, 1, 1]
        print('\n-------SEQUENTIAL NAIVE-------------------------------------')
        kwargs2=kwargs.copy()
        kwargs2['sequential']=True

        logic_fun=problem_logic_scheduling
        model_fun=case_2_scheduling_control_gdp_var_proc_time_min_proc_time
        m=model_fun(**kwargs2)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I for J in m.J if m.I_i_j_prod[I,J]==1}
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)



        # ## RUN THIS TO SOLVE
        m=sequential_non_iterative_2_case2(logic_fun,initialization_test,model_fun,kwargs2,ext_ref,provide_starting_initialization= False, subproblem_solver=nlp_solver,subproblem_solver_options=sub_options,tee = True, global_tee= True,rel_tol = 0)
        # ## RUN THIS TO RETRIEVE SOLUTION    

        m=initialize_model(m,from_feasible=True,feasible_model='case_2_scheduling_and_dynamics_solution')
        # NOTE: This print is only for the case where B is variable in scheduling
        # print('Minimum product composition that becomes infeasible',str(pe.value(m.CC['T2','U3',5][m.N['T2','U3',5].last()])))


        Sol_found=[]
        for I in m.I:
            for J in m.J:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print(Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJ_VAL=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE: ',str(OBJ_VAL))
# ###############################################################################
# #########--------------sequential iterative ------------------#################
# ###############################################################################
# ###############################################################################
    if case_2_sequential_iterative:
        print('\n-------SEQUENTIAL-------------------------------------')
        kwargs['sequential']=True

        logic_fun=problem_logic_scheduling
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential
        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I for J in m.J if m.I_i_j_prod[I,J]==1}
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        m,_=sequential_iterative_2_case2(logic_fun,initialization,model_fun,kwargs,ext_ref,provide_starting_initialization= False, subproblem_solver=nlp_solver,subproblem_solver_options=sub_options,tee = False, global_tee= True,rel_tol = 0)
        save=generate_initialization(m=m,model_name='case_2_sequential')
        Sol_found=[]
        for I in m.I:
            for J in m.J:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        # for I_J in m.I_J:
        #     Sol_found.append(1+round(pe.value(m.Nref[I_J])))

###############################################################################
#########--------------dbd-approx_sol_subproblems ------------------###########
###############################################################################
###############################################################################
    if case_2_MLD_BD_pruning_aprox_subproblems:
        print('\n-------DBD-approx solution of subproblems-------------------------------------')
        Sol_found=[1, 2, 2, 1, 1, 1, 1, 1, 2, 2, 5, 4, 1, 2, 1, 2] # from sequential iterative
        feas_model='case_2_sequential' # from sequential iterative
        kwargs['sequential']=True
        kwargs['x_initial']=Sol_found
        initialization=Sol_found
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'
        neigh=neighborhood_k_eq_2(len(Sol_found))



        logic_fun=problem_logic_scheduling_complete
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential


        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I for J in m.J if m.I_i_j_prod[I,J]==1}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()
        # NOTE: go to dsda_functions->solve_subproblem_aprox-> change approximate_solution as needed 
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,nlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=0,new_case=True,with_distillation=False,provide_starting_initialization=True,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_2_dbd_aprox_subproblems_'+minlp_solver+'_'+neighdef+'_all_neigh_Verified'
        save=generate_initialization(m=m,model_name=solname) 
        new_Sol_found=[]
        for I in m.I:
            for J in m.J:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            new_Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            new_Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print(new_Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES)) 
 

####CASE STUDY 2 WITH DISTILLATION DYNAMICS ###############################

    print('******CASE STUDY 2 WITH DISTILLATION DYNAMICS************')

# ###############################################################################
# #########--------------base case ------------------############################
# ###############################################################################
# ###############################################################################

    obj_Selected='profit_max'



    initialization=[1, 1, 1, 1, 1, 1, 1, 1]
  
    mip_solver='CPLEX'
    minlp_solver='DICOPT'
    nlp_solver='conopt4'
    transform='bigm'
    #tried 5 and no improvement. With 15 DICOT is unable, and now DSDA can solve the problem.
    last_disc=15
    last_time_h=5

    if minlp_solver=='dicopt' or minlp_solver=='DICOPT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
        print('DICOPT options:',sub_options)
    elif minlp_solver=='OCTERACT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','Option Threads =0;','Option SOLVER = OCTERACT;','$onecho > octeract.opt \n','LOCAL_SEARCH true\n','$offecho \n']}
    
    LO_PROC_TIME={('T1','U1'):0.5,('T2','U2'):0.1,('T2','U3'):0.1,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):0.1}
    UP_PROC_TIME={('T1','U1'):0.5,('T2','U2'):2,('T2','U3'):2,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):3}
    kwargs={'obj_type':obj_Selected,'last_disc_point':last_disc,'last_time_hours':last_time_h,'lower_t_h':LO_PROC_TIME,'upper_t_h':UP_PROC_TIME,'sequential':False}


    model_witn_distillation_dynamics=True

# ###############################################################################
# #########--------------sequential naive-------------###########################
# ###############################################################################
# ###############################################################################
    if case_2_sequential_naive_with_distillation:
        initialization_test=[1, 6, 6, 1, 1, 1, 1, 9] 
        print('\n-------SEQUENTIAL NAIVE-------------------------------------')
        kwargs2=kwargs.copy()
        kwargs2['sequential']=True

        logic_fun=problem_logic_scheduling
        model_fun=case_2_scheduling_control_gdp_var_proc_time_min_proc_time_with_distillation
        m=model_fun(**kwargs2)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I for J in m.J if m.I_i_j_prod[I,J]==1}
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)



        ## RUN THIS TO SOLVE
        m=sequential_non_iterative_2_case2(logic_fun,initialization_test,model_fun,kwargs2,ext_ref,provide_starting_initialization= False, subproblem_solver=nlp_solver,subproblem_solver_options=sub_options,tee = True, global_tee= True,rel_tol = 0, with_distillation=model_witn_distillation_dynamics)
        ## RUN THIS TO RETRIEVE SOLUTION    

        m=initialize_model(m,from_feasible=True,feasible_model='case_2_scheduling_and_dynamics_solution_with_distillation')

        Sol_found=[]
        for I in m.I:
            for J in m.J:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print(Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJECTIVE=TPC1+TPC2+TPC3+TMC-SALES

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJECTIVE: ',OBJECTIVE)

# ###############################################################################
# #########--------------sequential ------------------###########################
# ###############################################################################
# ###############################################################################
    if case_2_sequential_iterative_with_distillation:
        print('\n-------SEQUENTIAL-------------------------------------')
        kwargs['sequential']=True

        logic_fun=problem_logic_scheduling
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation
        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I for J in m.J if m.I_i_j_prod[I,J]==1}
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)
        m,_=sequential_iterative_2_case2(logic_fun,initialization,model_fun,kwargs,ext_ref,provide_starting_initialization= False, subproblem_solver=nlp_solver,subproblem_solver_options=sub_options,gams_output=False,tee = True, global_tee= True,rel_tol = 0,dynamic_dist_model=True)
        save=generate_initialization(m=m,model_name='case_2_sequential_with_distillation')
        # save=generate_initialization(m=m,model_name='case_2_sequential_with_distillation_discrete_processing_time')
        Sol_found=[]
        for I in m.I:
            for J in m.J:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m.minTau[I,J]+1)
        # for I_J in m.I_J:
        #     Sol_found.append(1+round(pe.value(m.Nref[I_J])))

# ###############################################################################
# #########--------------dicopt ----------------#################################
# ###############################################################################
# ###############################################################################
    if case_2_dicopt_with_distillation:
        print('\n-------DICOPT-------------------------------------')
        kwargs['sequential']=False
        kwargs['x_initial']=[1, 2, 3, 1, 1, 1, 1, 7, 2, 2, 4, 4, 1, 2, 1, 2] # from sequential iterative
        logic_fun=problem_logic_scheduling_complete
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation
        m=model_fun(**kwargs)
        m=initialize_model(m,from_feasible=True,feasible_model='case_2_sequential_with_distillation') 
        start=time.time()

        m=solve_with_minlp(m,transformation=transform,minlp=minlp_solver,minlp_options=sub_options,timelimit=86400,gams_output=False,tee=True,rel_tol=0)
        end=time.time()    
        solname='case_2_with_distillation_opt_'+minlp_solver
        save=generate_initialization(m=m,model_name=solname)

        if m.results.solver.termination_condition == 'infeasible' or m.results.solver.termination_condition == 'other' or m.results.solver.termination_condition == 'unbounded' or m.results.solver.termination_condition == 'invalidProblem' or m.results.solver.termination_condition == 'solverFailure' or m.results.solver.termination_condition == 'internalSolverError' or m.results.solver.termination_condition == 'error'  or m.results.solver.termination_condition == 'resourceInterrupt' or m.results.solver.termination_condition == 'licensingProblem' or m.results.solver.termination_condition == 'noSolution' or m.results.solver.termination_condition == 'noSolution' or m.results.solver.termination_condition == 'intermediateNonInteger': 
            m.dicopt_status='Infeasible'
        else:
            m.dicopt_status='Optimal'

        if m.dicopt_status=='Optimal':
            Sol_founddicopt=[]
            for I in m.I:
                for J in m.J:
                    if m.I_i_j_prod[I,J]==1:
                        for K in m.ordered_set[I,J]:
                            if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                                Sol_founddicopt.append(K-m.minTau[I,J]+1)
            for I_J in m.I_J:
                Sol_founddicopt.append(1+round(pe.value(m.Nref[I_J])))


            print('Objective DICOPT=',pe.value(m.obj),'best DICOPT=',Sol_founddicopt,'cputime DICOPT=',str(end-start))
        else:
            print('DICOPT infeasible')

            TPC1=pe.value(m.TCP1)
            TPC2=pe.value(m.TCP2)
            TPC3=pe.value(m.TCP3)
            TMC=pe.value(m.TMC)
            SALES=pe.value(m.SALES)
            OBJVAL=(TPC1+TPC2+TPC3+TMC-SALES)
            print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
            print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
            print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
            print('TMC: Total material cost: ',str(TMC))
            print('SALES: Revenue form selling products: ',str(SALES))
            print('OBJ:',str(OBJVAL))
###############################################################################
#########--------------dbd-approx_sol_subproblems ------------------###########
###############################################################################
###############################################################################
    if case_2_MLD_BD_pruning_aprox_subproblems_with_distillation:
        print('\n-------DBD-approx solution of subproblems-------------------------------------')
        Sol_found=[1, 2, 3, 1, 1, 1, 1, 7, 2, 2, 4, 4, 1, 2, 1, 2] # from sequential iterative
        # Sol_found=[1, 3, 2, 1, 1, 1, 1, 1, 2, 2, 6, 5, 1, 1, 1, 1] # from sequential naive
        feas_model='case_2_sequential_with_distillation' # from sequential iterative

        kwargs['sequential']=True
        kwargs['x_initial']=Sol_found
        initialization=Sol_found
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'
        neigh=neighborhood_k_eq_2(len(Sol_found))



        logic_fun=problem_logic_scheduling_complete
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation


        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I for J in m.J if m.I_i_j_prod[I,J]==1}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)

        start=time.time()
                                                                                                                                                                        ## TODO: this second model function is a version of the model with only scheduling constraints. Work on this!!!!!
        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,nlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=0,new_case=True,with_distillation=model_witn_distillation_dynamics,provide_starting_initialization=True,feasible_model=feas_model)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_2_dbd_with_distillation_aprox_subproblems_'+minlp_solver+'_'+neighdef+'_all_neigh_Verified'
        save=generate_initialization(m=m,model_name=solname) 
        new_Sol_found=[]
        for I in m.I:
            for J in m.J:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            new_Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            new_Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print(new_Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))  

###############################################################################
#########--------------dbd-approx_sol_subproblems from infeasible ----#########
###############################################################################
###############################################################################
    if case_2_MLD_BD_pruning_aprox_subproblems_from_infeasible_with_distillation:
        print('\n-------DBD-approx solution of subproblems from infeasible-------------------------------------')



        logic_fun=problem_logic_scheduling_complete
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation
        model_fun_scheduling=case_2_scheduling_only_lower_bound_tau
        infinity_val=1e+4
        maxiter=10000
        neighdef='2'
        kwargs['sequential']=True


        m_scheduling_only=model_fun_scheduling(**kwargs)
        sub_options_cplex_Feas={'add_options':['GAMS_MODEL.optfile = 1;','$onecho > cplex.opt \n','$offecho \n']} 
        m_scheduling_only = solve_with_minlp(m_scheduling_only,transformation='bigm',minlp='cplex',minlp_options=sub_options_cplex_Feas,timelimit=360000000,gams_output=False,tee=True,rel_tol=0)
        Sol_found=[]
        for I in m_scheduling_only.I:
            for J in m_scheduling_only.J:
                if m_scheduling_only.I_i_j_prod[I,J]==1:
                    for K in m_scheduling_only.ordered_set[I,J]:
                        if round(pe.value(m_scheduling_only.YR_disjunct[I,J][K].indicator_var))==1:
                            Sol_found.append(K-m_scheduling_only.minTau[I,J]+1)
        for I_J in m_scheduling_only.I_J:
            Sol_found.append(1+round(pe.value(m_scheduling_only.Nref[I_J])))

        print('Initialization=',Sol_found)



        initialization=Sol_found
        neigh=neighborhood_k_eq_2(len(Sol_found))




        m=model_fun(**kwargs)
        ext_ref={m.YR[I,J]:m.ordered_set[I,J] for I in m.I for J in m.J if m.I_i_j_prod[I,J]==1}
        ext_ref.update({m.YR2[I_J]:m.ordered_set2[I_J] for I_J in m.I_J})
        [reformulation_dict, number_of_external_variables, lower_bounds, upper_bounds]=get_external_information(m,ext_ref,tee=True)


        start=time.time()

        [important_info,important_info_preprocessing,D,x_actual,m]=run_function_dbd_aprox(initialization,infinity_val,nlp_solver,neigh,maxiter,ext_ref,logic_fun,model_fun,model_fun_scheduling,kwargs,use_random=False,sub_solver_opt=sub_options, tee=True,rel_tol=0,new_case=True,with_distillation=model_witn_distillation_dynamics,provide_starting_initialization=False)
        
        print('Objective value: ',str(pe.value(m.obj)))
        print('Objective value: ',str(important_info['m3_s3'][0])+'; time= ',str(important_info['m3_s3'][1]))

        end=time.time()

        solname='case_2_dbd_with_distillation_aprox_subproblems_'+minlp_solver+'_'+neighdef+'_all_neigh_Verified_from_infeasible'
        save=generate_initialization(m=m,model_name=solname) 
        new_Sol_found=[]
        for I in m.I:
            for J in m.J:
                if m.I_i_j_prod[I,J]==1:
                    for K in m.ordered_set[I,J]:
                        if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                            new_Sol_found.append(K-m.minTau[I,J]+1)
        for I_J in m.I_J:
            new_Sol_found.append(1+round(pe.value(m.Nref[I_J])))
        print(new_Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)

        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES)) 
