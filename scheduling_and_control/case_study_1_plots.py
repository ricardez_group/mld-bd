from __future__ import division
import sys
sys.path.append('C:/Users/dlinanro/Desktop/mld-bd/') 
from functions.d_bd_functions import run_function_dbd,run_function_dbd_aprox
from functions.dsda_functions import get_external_information,external_ref,solve_subproblem,generate_initialization,initialize_model,solve_with_gdpopt,solve_with_minlp
import pyomo.environ as pe
from pyomo.gdp import Disjunct, Disjunction
import math
from pyomo.opt.base.solvers import SolverFactory
import io
import time
from functions.dsda_functions import neighborhood_k_eq_2,get_external_information,external_ref,solve_subproblem,solve_subproblem_aprox,generate_initialization,initialize_model,solve_with_dsda,solve_with_dsda_aprox,sequential_iterative_1,sequential_iterative_2,neighborhood_k_eq_l_natural,neighborhood_k_eq_m_natural,neighborhood_k_eq_l_natural_modified,solve_with_dsda_aprox_tau_only
import logging
# from case_study_1_model_reduced import scheduling_and_control,problem_logic_scheduling
# from case_study_1_model import scheduling_and_control as scheduling_and_control_GDP 
# from case_study_1_model import scheduling_and_control_gdp_N as scheduling_and_control_GDP_complete
# from case_study_1_model import scheduling_and_control_gdp_N_approx as scheduling_and_control_GDP_complete_approx
from case_study_1_model import scheduling_and_control_gdp_N_approx_only_tau as scheduling_and_control_GDP_complete_approx_tau_only
from case_study_1_model import scheduling_and_control_gdp_N_approx_sequential
from case_study_1_model import scheduling_and_control_gdp_N_solvegdp_simpler
from case_study_1_model import problem_logic_scheduling,problem_logic_scheduling_dummy
from case_study_1_model import scheduling_only_gdp_N_solvegdp_simpler,scheduling_only_gdp_N_solvegdp_simpler_lower_bound_tau
import matplotlib.pyplot as plt
import os

if __name__ == "__main__":
    #Do not show warnings
    logging.getLogger('pyomo').setLevel(logging.ERROR)

    mip_solver='CPLEX'
    minlp_solver='DICOPT'
    nlp_solver='conopt4'
    transform='bigm'
    if minlp_solver=='dicopt' or minlp_solver=='DICOPT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=0;','$onecho > dicopt.opt \n','maxcycles 20000 \n','stop 3 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
    elif minlp_solver=='OCTERACT':
        sub_options={'add_options':['GAMS_MODEL.optfile = 1;','Option Threads =0;','Option SOLVER = OCTERACT;','$onecho > octeract.opt \n','LOCAL_SEARCH true\n','$offecho \n']}
    
    # Decide if plots are going to be generated for the long (28 h) or short (14 h) scheduling horizon
    plot_case='long' #'long'  'long'
    if plot_case =='short':
        # CASE STUDY 1 (SHORT)
        kwargs={}
    else:
        # # CASE STUDY 1 (LONG)
        kwargs={'last_time_hours':28,'demand_p1_kmol':2,'demand_p2_kmol':2}

    model_fun=scheduling_and_control_gdp_N_solvegdp_simpler
    m=model_fun(**kwargs)

    # Experiments in MLDBD_experiments.py or DSDA_experiments.py save files with result information. These files can be read here to plot.
    # init_name corresponds to the name of the file with the results that are going to be included in the plots.

    # init_name='case1_Figure3A_FigureS1B_FigureS1D_MLDBD'
    init_name='case1_Figure3B_FigureS1A_FigureS1C_SIA' 



    m=initialize_model(m,from_feasible=True,feasible_model=init_name) 
# # ####--------Objective function summary---------------------------------
    TPC1=sum(sum(sum(  m.fixed_cost[I,J]*pe.value(m.X[I,J,T]) for J in m.J)for I in m.I)for T in m.T)
    TPC2=sum(sum(sum( m.variable_cost[I,J]*pe.value(m.B[I,J,T]) for J in m.J_noDynamics) for I in m.I_noDynamics) for T in m.T)
    TPC3=sum(sum(sum(pe.value(m.X[I,J,T])*(m.hot_cost*pe.value(m.Integral_hot[I,J][m.N[I,J].last()])   +  m.cold_cost*pe.value(m.Integral_cold[I,J][m.N[I,J].last()])  ) for T in m.T) for I in m.I_reactions)for J in m.J_reactors)
    TMC=sum( m.raw_cost[K]*(m.S0[K]-pe.value(m.S[K,m.lastT])) for K in m.K_inputs)
    SALES=sum( m.revenue[K]*pe.value(m.S[K,m.lastT])  for K in m.K_products)
    OBJVAL=(TPC1+TPC2+TPC3+TMC-SALES)
    print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
    print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
    print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
    print('TMC: Total material cost: ',str(TMC))
    print('SALES: Revenue form selling products: ',str(SALES))
    print('OBJ:',str(OBJVAL))
    print('----')
    print('TCP1 gams:',str(pe.value(m.TCP1)))
    print('TCP2 gams:',str(pe.value(m.TCP2)))
    print('TCP3 gams:',str(pe.value(m.TCP3)))
    print('TMC gams:',str(pe.value(m.TMC)))
    print('SALES gams:',str(pe.value(m.SALES)))


#######-------plots------------------------
    newpath = r'figures/case_1_example'
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    for I in m.I_reactions:
        for J in m.J_reactors:
            case=(I,J)
            t=[]
            c1=[]
            c2=[]
            c3=[]
            Tr=[]
            Tj=[]
            Fhot=[]
            Fcold=[]
            for N in m.N[case]:
                t.append(N*m.varTime[I,J].value*60*60)
                Tr.append(m.TRvar[case][N].value)
                Tj.append(m.TJvar[case][N].value)
                Fhot.append(m.Fhot[case][N].value*(1/60)*(1/60))
                Fcold.append(m.Fcold[case][N].value*(1/60)*(1/60))
                c1.append( m.Cvar[case][N,list(m.Q_balance[I])[0]].value*1000)
                c2.append( m.Cvar[case][N,list(m.Q_balance[I])[1]].value*1000)
                c3.append( m.Cvar[case][N,list(m.Q_balance[I])[2]].value*1000)
                
                
            plt.plot(t, c1,label=list(m.Q_balance[I])[0],color='red')
            plt.plot(t, c2,label=list(m.Q_balance[I])[1],color='green')
            plt.plot(t, c3,label=list(m.Q_balance[I])[2],color='blue')
            plt.xlabel('Time [s]')
            plt.ylabel('$Concentration [mol/m^{3}]$')
            title=case[0]+' in '+case[1]+' Concentration'
            plt.title(case[0]+' in '+case[1])
            plt.legend()
            # plt.show()
            plt.savefig("figures/case_1_example/"+title+".svg") 
            plt.clf()
            plt.cla()
            plt.close()

            plt.plot(t,Tr,label='T_reactor',color='red')
            plt.plot(t,Tj,label='T_jacket',color='blue')
            plt.xlabel('Time [s]')
            plt.ylabel('Temperature [K]')
            title=case[0]+' in '+case[1]+' Temperature'
            plt.title(case[0]+' in '+case[1])
            plt.legend()
            # plt.show()
            plt.savefig("figures/case_1_example/"+title+".svg") 
            plt.clf()
            plt.cla()
            plt.close()
            
            plt.plot(t, Fhot,label='F_hot',color='red')
            plt.plot(t,Fcold,label='F_cold',color='blue')
            plt.xlabel('Time [s]')
            plt.ylabel('Flow rate $[m^{3}/s]$')
            title=case[0]+' in '+case[1]+' Flow rate'
            plt.title(case[0]+' in '+case[1])
            plt.legend()
            # plt.show()    
            plt.savefig("figures/case_1_example/"+title+".svg") 
            plt.clf()
            plt.cla()
            plt.close()
    # plot of states
    for k in m.K:
        t_pro=[]
        state=[]
        for t in m.T:
            t_pro.append(m.t_p[t]*60*60)
            state.append(pe.value(m.S[k,t]))

        plt.plot(t_pro, state,color='red')
        plt.xlabel('Time [s]')
        plt.ylabel('State level $[m^{3}]$')
        title='state '+k
        plt.title(title)
        # plt.show()
        plt.savefig("figures/case_1_example/"+title+".svg") 
        plt.clf()
        plt.cla()
        plt.close()

    #--------------------------------- Gantt plot--------------------------------------------
    fig, gnt = plt.subplots(figsize=(11, 5), sharex=True, sharey=False)
    # Setting Y-axis limits
    gnt.set_ylim(8, 62)
    
    # Setting X-axis limits
    gnt.set_xlim(0, m.lastT.value*m.delta.value*60*60)
    
    # Setting labels for x-axis and y-axis
    gnt.set_xlabel('Time [s]')
    gnt.set_ylabel('Units')
    
    # Setting ticks on y-axis
    gnt.set_yticks([15, 25, 35, 45, 55])
    # Labelling tickes of y-axis
    gnt.set_yticklabels(['Pack', 'Sep', 'Rs', 'Rl','Mix'])
    
    
    # Setting graph attribute
    gnt.grid(False)
    
    # Declaring bars in schedule
    height=9
    already_used=[]
    for j in m.J:
        if j=='Mix':
            lower_y_position=50
        elif j=='R_large':
            lower_y_position=40    
        elif j=='R_small':
            lower_y_position=30    
        elif j=='Sep':
            lower_y_position=20
        elif j=='Pack':
            lower_y_position=10
        for i in m.I:
            if i=='Mix':
                bar_color='tab:red'
            elif i=='R1':
                bar_color='tab:green'    
            elif i=='R2':
                bar_color='tab:blue'    
            elif i=='R3':
                bar_color='tab:orange' 
            elif i=='Sep':
                bar_color='tab:olive'
            elif i=='Pack1':
                bar_color='tab:purple'                
            elif i=='Pack2':
                bar_color='teal'
            for t in m.T:
                try:
                    if i in m.I_reactions and j in m.J_reactors:
                        if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                            gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                            gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                            already_used.append(i)
                        elif round(pe.value(m.X[i,j,t]))==1:
                            gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                            gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                                
                    else:
                        if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                            gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                            gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                            already_used.append(i)
                        elif round(pe.value(m.X[i,j,t]))==1:
                            gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                            gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                        
                except:
                    pass 
    gnt.tick_params(axis='both', which='major', labelsize=15)
    gnt.tick_params(axis='both', which='minor', labelsize=15) 
    gnt.yaxis.label.set_size(15)
    gnt.xaxis.label.set_size(15)
    plt.legend()
    # plt.show()
    plt.savefig("figures/case_1_example/gantt_minlp.svg")   
    plt.clf()
    plt.cla()
    plt.close()
