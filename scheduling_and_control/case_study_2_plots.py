from __future__ import division
from pickle import TRUE

import sys
sys.path.append('C:/Users/dlinanro/Desktop/mld-bd/') 
from functions.d_bd_functions import run_function_dbd
from functions.dsda_functions import get_external_information,external_ref,solve_subproblem,generate_initialization,initialize_model,solve_with_minlp
import pyomo.environ as pe
from pyomo.gdp import Disjunct, Disjunction
import math
from pyomo.opt.base.solvers import SolverFactory
import io
import time
from functions.dsda_functions import neighborhood_k_eq_all,neighborhood_k_eq_l_natural,neighborhood_k_eq_2,get_external_information,external_ref,solve_subproblem,generate_initialization,initialize_model,solve_with_dsda
import logging
from case_study_2_model import problem_logic_scheduling,case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential,case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation
import os
import matplotlib.pyplot as plt
from math import fabs

if __name__ == "__main__":
    #Do not show warnings
    logging.getLogger('pyomo').setLevel(logging.ERROR)

###############################################################################
#########--------------base case ------------------############################
###############################################################################
###############################################################################

    obj_Selected='profit_max'
    mip_solver='CPLEX'
    minlp_solver='DICOPT'
    nlp_solver='conopt4'
    transform='bigm'
    last_disc=15
    last_time_h=5
    logic_fun=problem_logic_scheduling
    with_distillation=True
    sequential_naive=False #true if i am ploting results from sequential naive
    sub_options={'add_options':['GAMS_MODEL.optfile = 1;','GAMS_MODEL.threads=7;','$onecho > dicopt.opt \n','maxcycles 20000 \n','nlpsolver '+nlp_solver,'\n','$offecho \n','option mip='+mip_solver+';\n']}
    if not with_distillation:
        LO_PROC_TIME={('T1','U1'):0.5,('T2','U2'):0.1,('T2','U3'):0.1,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):1.5}
        UP_PROC_TIME={('T1','U1'):0.5,('T2','U2'):2,('T2','U3'):2,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):1.5}
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential
    else:
        LO_PROC_TIME={('T1','U1'):0.5,('T2','U2'):0.1,('T2','U3'):0.1,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):0.1}
        UP_PROC_TIME={('T1','U1'):0.5,('T2','U2'):2,('T2','U3'):2,('T3','U2'):1,('T3','U3'):2.5,('T4','U2'):1,('T4','U3'):5,('T5','U4'):3} 
        model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation       
    kwargs={'obj_type':obj_Selected,'last_disc_point':last_disc,'last_time_hours':last_time_h,'lower_t_h':LO_PROC_TIME,'upper_t_h':UP_PROC_TIME,'sequential':False}        
    m=model_fun(**kwargs)


    
    # Experiments in MLDBD_experiments.py or DSDA_experiments.py save files with result information. These files can be read here to plot.
    # init_name corresponds to the name of the file with the results that are going to be included in the plots.

    # init_name='case2_Figure4A_SIA'
    # init_name='case2_Figure4B_Figure5A_Figure5C_IIA'
    init_name='case2_Figure4C_Figure5B_Figure5D_MLDBD'



    m=initialize_model(m,from_feasible=True,feasible_model=init_name) 

    Sol_found=[]
    for I in m.I:
        for J in m.J:
            if m.I_i_j_prod[I,J]==1:
                for K in m.ordered_set[I,J]:
                    if round(pe.value(m.YR_disjunct[I,J][K].indicator_var))==1:
                        Sol_found.append(K-m.minTau[I,J]+1)
    for I_J in m.I_J:
        Sol_found.append(1+round(pe.value(m.Nref[I_J])))

    if not sequential_naive or sequential_naive:
        print('Objective =',pe.value(m.obj),'best =',Sol_found)
        TPC1=pe.value(m.TCP1)
        TPC2=pe.value(m.TCP2)
        TPC3=pe.value(m.TCP3)
        TMC=pe.value(m.TMC)
        SALES=pe.value(m.SALES)
        OBJVAL=(TPC1+TPC2+TPC3+TMC-SALES)
        print('TPC: Fixed costs for all unit-tasks: ',str(TPC1))   
        print('TPC: Variable cost for unit-tasks that do not consider dynamics: ', str(TPC2))
        print('TPC: Variable cost for unit-tasks that do consider dynamics: ',str(TPC3))
        print('TMC: Total material cost: ',str(TMC))
        print('SALES: Revenue form selling products: ',str(SALES))
        print('OBJ:',str(OBJVAL))
        if with_distillation:
            cost_distillation=5/100
            DISTIl_COST=sum(sum(sum(pe.value(m.X[I, J, T])*( cost_distillation*pe.value(m.dist_models[I,J,T].I_V[m.dist_models[I,J,T].T.last()])  ) for T in m.T) for I in m.I_distil)for J in m.J_distil)
            print('TPC: Variable cost for unit-tasks that do consider dynamics, distillation only:', str(DISTIl_COST))


#######-------plots------------------------
    newpath = r'figures/case_2_example'
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    if not with_distillation:
        if sequential_naive:
            for I in m.I_dynamics:
                for J in m.J_dynamics:
                    for T in m.T:
                        if pe.value(m.X[I,J,T])==1: 
                            case=(I,J,T)
                            t=[]
                            CA=[]
                            CB=[]
                            CC=[]
                            Tr=[]
                            Tj=[]
                            Fhot=[]
                            Fcold=[]
                            u_input=[]
                            for N in m.N[case]:
                                t.append(N*m.varTime[case].value*(60)*(60)) #Seconds
                                Tr.append(m.TRvar[case][N].value)
                                Tj.append(m.TJvar[case][N].value)
                                Fhot.append(m.Fhot[case][N].value*(1/60)*(1/60)) #m^3/s
                                Fcold.append(m.Fcold[case][N].value*(1/60)*(1/60)) #m^3/s
                                CA.append( m.CA[case][N].value*1000)  #mol/m^3
                                CB.append( m.CB[case][N].value*1000)
                                CC.append( m.CC[case][N].value*1000)
                                u_input.append(m.u_input[case][N].value*(1/60)*(1/60)) #m^3/s
                                
                                
                            plt.plot(t, CA,label='CA',color='red')
                            plt.plot(t, CB,label='CB',color='green')
                            plt.plot(t, CC,label='CC',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Concentration $[mol/m^{3}]$')
                            title=case[0]+' in '+case[1]+' Concentration'
                            plt.title(case[0]+' in '+case[1])
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t,Tr,label=r'$T_{reactor}$',color='red')
                            plt.plot(t,Tj,label=r'$T_{jacket}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Temperature [K]')
                            title=case[0]+' in '+case[1]+' Temperature'
                            plt.title(case[0]+' in '+case[1])
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()
                            
                            plt.plot(t, Fhot,label=r'$F_{hot}$',color='red')
                            plt.plot(t,Fcold,label=r'$F_{cold}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate'
                            plt.title(case[0]+' in '+case[1])
                            plt.legend()
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t, u_input,color='red')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate of B $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate of B'
                            plt.title(case[0]+' in '+case[1])
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()
        else: 
            for I in m.I_dynamics:
                for J in m.J_dynamics:
                    for T in m.T:
                        if pe.value(m.X[I,J,T])==1: 
                            case=(I,J,T)
                            t=[]
                            CA=[]
                            CB=[]
                            CC=[]
                            Tr=[]
                            Tj=[]
                            Fhot=[]
                            Fcold=[]
                            u_input=[]
                            for N in m.N[case]:
                                t.append(N*m.varTime[case].value*(60)*(60))
                                Tr.append(m.TRvar[case][N].value)
                                Tj.append(m.TJvar[case][N].value)
                                Fhot.append(m.Fhot[case][N].value*(1/60)*(1/60))
                                Fcold.append(m.Fcold[case][N].value*(1/60)*(1/60))
                                CA.append( m.CA[case][N].value*1000)
                                CB.append( m.CB[case][N].value*1000)
                                CC.append( m.CC[case][N].value*1000)
                                u_input.append(m.u_input[case][N].value*(1/60)*(1/60))
                                
                                
                            plt.plot(t, CA,label='CA',color='red')
                            plt.plot(t, CB,label='CB',color='green')
                            plt.plot(t, CC,label='CC',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Concentration $[mol/m^{3}]$')
                            title=case[0]+' in '+case[1]+' Concentration'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t,Tr,label=r'$T_{reactor}$',color='red')
                            plt.plot(t,Tj,label=r'$T_{jacket}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Temperature [K]')
                            title=case[0]+' in '+case[1]+' Temperature'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()
                            
                            plt.plot(t, Fhot,label=r'$F_{hot}$',color='red')
                            plt.plot(t,Fcold,label=r'$F_{cold}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t, u_input,color='red')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate of B $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate of B'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

    else:
        if sequential_naive:
            for I in m.I_dynamics:
                for J in m.J_dynamics:
                    for T in m.T:
                        if pe.value(m.X[I,J,T])==1: 
                            case=(I,J,T)
                            t=[]
                            CA=[]
                            CB=[]
                            CC=[]
                            Tr=[]
                            Tj=[]
                            Fhot=[]
                            Fcold=[]
                            u_input=[]
                            for N in m.N[case]:
                                t.append(N*m.varTime[case].value*60*60)
                                Tr.append(m.TRvar[case][N].value)
                                Tj.append(m.TJvar[case][N].value)
                                Fhot.append(m.Fhot[case][N].value*(1/60)*(1/60))
                                Fcold.append(m.Fcold[case][N].value*(1/60)*(1/60))
                                CA.append( m.CA[case][N].value*1000)
                                CB.append( m.CB[case][N].value*1000)
                                CC.append( m.CC[case][N].value*1000)
                                u_input.append(m.u_input[case][N].value*(1/60)*(1/60))
                                
                                
                            plt.plot(t, CA,label='CA',color='red')
                            plt.plot(t, CB,label='CB',color='green')
                            plt.plot(t, CC,label='CC',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Concentration $[mol/m^{3}]$')
                            title=case[0]+' in '+case[1]+' Concentration'
                            plt.title(case[0]+' in '+case[1])
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t,Tr,label=r'$T_{reactor}$',color='red')
                            plt.plot(t,Tj,label=r'$T_{jacket}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Temperature [K]')
                            title=case[0]+' in '+case[1]+' Temperature'
                            plt.title(case[0]+' in '+case[1])
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()
                            
                            plt.plot(t, Fhot,label=r'$F_{hot}$',color='red')
                            plt.plot(t,Fcold,label=r'$F_{cold}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate'
                            plt.title(case[0]+' in '+case[1])
                            plt.legend()
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t, u_input,color='red')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate of B $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate of B'
                            plt.title(case[0]+' in '+case[1])
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()
            for I in m.I_distil:
                for J in m.J_distil:
                    for T in m.T:
                        if pe.value(m.X[I,J,T])==1: 
                            case=(I,J,T)
                            tdist=[]
                            Dist=[]
                            Boil_up=[]
                            Reflux_rate=[]
                            x_instantaneous=[]
                            x_accumulated=[]
                            Reboiler_hold_up=[]
                            Product_accumulated=[]

                            for N in m.dist_models[case].T:
                                tdist.append(N*m.varTime[case].value*60*60) #seconds
                                Dist.append(m.dist_models[case].D[N].value*(1/60)*(1/60)) #m^3/s
                                Boil_up.append(m.dist_models[case].V[N].value*(1/60)*(1/60)) #m^3/s
                                Reflux_rate.append(m.dist_models[case].R[N].value*(1/60)*(1/60)) #m^3/s
                                x_instantaneous.append(m.dist_models[case].x[m.dist_models[case].N.last(), N].value) #mol/mol
                                x_accumulated.append(m.dist_models[case].xd_average[N].value) #mol/mol
                                Reboiler_hold_up.append(m.dist_models[case].HB[N].value) #m^3
                                Product_accumulated.append(m.dist_models[case].I2[N].value) #m^3

                            plt.plot(tdist, x_instantaneous,label='Mole fraction of desired product (distillate)',color='red')
                            plt.plot(tdist,  x_accumulated,label='Mole fraction of desired product (reciever)',color='green')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Mole fraction $[mol/mol]$')
                            title=case[0]+' in '+case[1]+' Mole fraction'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            ax1 = plt.subplot()
                            l1=ax1.plot(tdist,Boil_up,label='Boil-up rate',color='red')
                            ax2=ax1.twinx()
                            l2=ax2.plot(tdist,Reflux_rate,label='Reflux rate',color='blue')
                            ax1.set_ylabel(r'Boil-up rate $[m^{3}/s]$',color='red')
                            ax2.set_ylabel(r'Reflux rate $[m^{3}/s]$',color='blue')                            
                            title=case[0]+' in '+case[1]+' Boil-up and reflux'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            # plt.legend()
                            # plt.show()
                            ax1.set_xlabel(r'Time [s]',color='black')
                            # plt.xlabel(r'Time [s]') 
                            plt.tight_layout()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(tdist, Reboiler_hold_up,label='Reboiler level',color='red')
                            plt.plot(tdist,  Product_accumulated,label='Reciever level',color='green')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Volume $[m^{3}]$')
                            title=case[0]+' in '+case[1]+' distillation levels'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()
        else: 
            for I in m.I_dynamics:
                for J in m.J_dynamics:
                    for T in m.T:
                        if pe.value(m.X[I,J,T])==1: 
                            case=(I,J,T)
                            t=[]
                            CA=[]
                            CB=[]
                            CC=[]
                            Tr=[]
                            Tj=[]
                            Fhot=[]
                            Fcold=[]
                            u_input=[]
                            for N in m.N[case]:
                                t.append(N*m.varTime[case].value*60*60)
                                Tr.append(m.TRvar[case][N].value)
                                Tj.append(m.TJvar[case][N].value)
                                Fhot.append(m.Fhot[case][N].value*(1/60)*(1/60))
                                Fcold.append(m.Fcold[case][N].value*(1/60)*(1/60))
                                CA.append( m.CA[case][N].value*1000)
                                CB.append( m.CB[case][N].value*1000)
                                CC.append( m.CC[case][N].value*1000)
                                u_input.append(m.u_input[case][N].value*(1/60)*(1/60))
                                
                                
                            plt.plot(t, CA,label='CA',color='red')
                            plt.plot(t, CB,label='CB',color='green')
                            plt.plot(t, CC,label='CC',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Concentration $[mol/m^{3}]$')
                            title=case[0]+' in '+case[1]+' Concentration'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t,Tr,label=r'$T_{reactor}$',color='red')
                            plt.plot(t,Tj,label=r'$T_{jacket}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Temperature [K]')
                            title=case[0]+' in '+case[1]+' Temperature'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()
                            
                            plt.plot(t, Fhot,label=r'$F_{hot}$',color='red')
                            plt.plot(t,Fcold,label=r'$F_{cold}$',color='blue')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(t, u_input,color='red')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Flow rate of B $[m^{3}/s]$')
                            title=case[0]+' in '+case[1]+' Flow rate of B'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            # plt.show()    
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()       
            for I in m.I_distil:
                for J in m.J_distil:
                    for T in m.T:
                        if pe.value(m.X[I,J,T])==1: 
                            case=(I,J,T)
                            tdist=[]
                            Dist=[]
                            Boil_up=[]
                            Reflux_rate=[]
                            x_instantaneous=[]
                            x_accumulated=[]
                            Reboiler_hold_up=[]
                            Product_accumulated=[]

                            for N in m.dist_models[case].T:
                                tdist.append(N*m.varTime[case].value*60*60)
                                Dist.append(m.dist_models[case].D[N].value*(1/60)*(1/60))
                                Boil_up.append(m.dist_models[case].V[N].value*(1/60)*(1/60))
                                Reflux_rate.append(m.dist_models[case].R[N].value*(1/60)*(1/60))
                                x_instantaneous.append(m.dist_models[case].x[m.dist_models[case].N.last(), N].value)
                                x_accumulated.append(m.dist_models[case].xd_average[N].value)
                                Reboiler_hold_up.append(m.dist_models[case].HB[N].value)
                                Product_accumulated.append(m.dist_models[case].I2[N].value)

                            plt.plot(tdist, x_instantaneous,label='Mole fraction of desired product (distillate)',color='red')
                            plt.plot(tdist,  x_accumulated,label='Mole fraction of desired product (reciever)',color='green')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Mole fraction $[mol/mol]$')
                            title=case[0]+' in '+case[1]+' Mole fraction'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            ax1 = plt.subplot()
                            l1=ax1.plot(tdist,Boil_up,label='Boil-up rate',color='red')
                            ax2=ax1.twinx()
                            l2=ax2.plot(tdist,Reflux_rate,label='Reflux rate',color='blue')

                            ax1.set_ylabel(r'Boil-up rate $[m^{3}/s]$',color='red')
                            ax2.set_ylabel(r'Reflux rate $[m^{3}/s]$',color='blue')                            
                            title=case[0]+' in '+case[1]+' Boil-up and reflux'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            # plt.legend()
                            # plt.show()
                            # plt.xlabel(r'Time [s]') 
                            ax1.set_xlabel(r'Time [s]',color='black')
                            plt.tight_layout()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

                            plt.plot(tdist, Reboiler_hold_up,label='Reboiler level',color='red')
                            plt.plot(tdist,  Product_accumulated,label='Reciever level',color='green')
                            plt.xlabel(r'Time [s]')
                            plt.ylabel(r'Volume $[m^{3}]$')
                            title=case[0]+' in '+case[1]+' distillation levels'+' at '+str(m.t_p[T]*60*60)+' s'
                            plt.title(case[0]+' in '+case[1]+' at '+str(m.t_p[T]*60*60)+' s')
                            plt.legend()
                            # plt.show()
                            plt.savefig("figures/case_2_example/"+title+".svg") 
                            plt.clf()
                            plt.cla()
                            plt.close()

    if not with_distillation:
        if sequential_naive:
            model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential
            m=model_fun(**kwargs)
            m=initialize_model(m,from_feasible=True,feasible_model=init_name) 

            # plot of states
            for k in m.K:
                t_pro=[]
                state=[]
                for t in m.T:
                    t_pro.append(m.t_p[t]*60*60)
                    state.append(pe.value(m.S[k,t]))

                plt.plot(t_pro, state,color='red')
                plt.xlabel('Time [s]')
                plt.ylabel('State level $[m^{3}]$')
                title='state '+k
                plt.title(title)
                # plt.show()
                plt.savefig("figures/case_2_example/"+title+".svg") 
                plt.clf()
                plt.cla()
                plt.close()

            #--------------------------------- Gantt plot--------------------------------------------
            fig, gnt = plt.subplots(figsize=(11, 5), sharex=True, sharey=False)
            # Setting Y-axis limits
            gnt.set_ylim(8, 52) #TODO: change depending case study
            
            # Setting X-axis limits
            gnt.set_xlim(0, m.lastT.value*m.delta.value*60*60)
            
            # Setting labels for x-axis and y-axis
            gnt.set_xlabel('Time [s]')
            gnt.set_ylabel('Units')
            
            # Setting ticks on y-axis
            gnt.set_yticks([15, 25, 35, 45]) #TODO: change depending case study
            # Labelling tickes of y-axis
            gnt.set_yticklabels(['U4', 'U3', 'U2', 'U1']) #TODO: change depending case study
            
            
            # Setting graph attribute
            gnt.grid(False)
            
            # Declaring bars in schedule
            height=9
            already_used=[]
            for j in m.J:

                if j=='U1':
                    lower_y_position=40    
                elif j=='U2':
                    lower_y_position=30    
                elif j=='U3':
                    lower_y_position=20
                elif j=='U4':
                    lower_y_position=10
                for i in m.I:
                    if i=='T1':
                        bar_color='tab:red'
                    elif i=='T2':
                        bar_color='tab:green'    
                    elif i=='T3':
                        bar_color='tab:blue'    
                    elif i=='T4':
                        bar_color='tab:orange' 
                    elif i=='T5':
                        bar_color='tab:olive'
                    for t in m.T:
                        try:
                            if i in m.I_dynamics and j in m.J_dynamics:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                                              
                            else:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                        

                        except:
                            pass 
            gnt.tick_params(axis='both', which='major', labelsize=15)
            gnt.tick_params(axis='both', which='minor', labelsize=15) 
            gnt.yaxis.label.set_size(15)
            gnt.xaxis.label.set_size(15)
            plt.legend()
            # plt.show()
            plt.savefig("figures/case_2_example/gantt_minlp.svg")   
            plt.clf()
            plt.cla()
            plt.close()


        else:

            # plot of states
            for k in m.K:
                t_pro=[]
                state=[]
                for t in m.T:
                    t_pro.append(m.t_p[t]*60*60)
                    state.append(pe.value(m.S[k,t]))

                plt.plot(t_pro, state,color='red')
                plt.xlabel('Time [s]')
                plt.ylabel('State level $[m^{3}]$')
                title='state '+k
                plt.title(title)
                # plt.show()
                plt.savefig("figures/case_2_example/"+title+".svg") 
                plt.clf()
                plt.cla()
                plt.close()

            #--------------------------------- Gantt plot--------------------------------------------
            fig, gnt = plt.subplots(figsize=(11, 5), sharex=True, sharey=False)
            # Setting Y-axis limits
            gnt.set_ylim(8, 52) #TODO: change depending case study
            
            # Setting X-axis limits
            gnt.set_xlim(0, m.lastT.value*m.delta.value*60*60)
            
            # Setting labels for x-axis and y-axis
            gnt.set_xlabel('Time [s]')
            gnt.set_ylabel('Units')
            
            # Setting ticks on y-axis
            gnt.set_yticks([15, 25, 35, 45]) #TODO: change depending case study
            # Labelling tickes of y-axis
            gnt.set_yticklabels(['U4', 'U3', 'U2', 'U1']) #TODO: change depending case study
            
            
            # Setting graph attribute
            gnt.grid(False)
            
            # Declaring bars in schedule
            height=9
            already_used=[]
            for j in m.J:

                if j=='U1':
                    lower_y_position=40    
                elif j=='U2':
                    lower_y_position=30    
                elif j=='U3':
                    lower_y_position=20
                elif j=='U4':
                    lower_y_position=10
                for i in m.I:
                    if i=='T1':
                        bar_color='tab:red'
                    elif i=='T2':
                        bar_color='tab:green'    
                    elif i=='T3':
                        bar_color='tab:blue'    
                    elif i=='T4':
                        bar_color='tab:orange' 
                    elif i=='T5':
                        bar_color='tab:olive'
                    for t in m.T:
                        try:
                            if i in m.I_dynamics and j in m.J_dynamics:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                                              
                            else:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                        

                        except:
                            pass 
            gnt.tick_params(axis='both', which='major', labelsize=15)
            gnt.tick_params(axis='both', which='minor', labelsize=15) 
            gnt.yaxis.label.set_size(15)
            gnt.xaxis.label.set_size(15)
            plt.legend()
            # plt.show()
            plt.savefig("figures/case_2_example/gantt_minlp.svg")   
            plt.clf()
            plt.cla()
            plt.close()

    else:

        if sequential_naive:
            model_fun=case_2_scheduling_control_gdp_var_proc_time_simplified_for_sequential_with_distillation
            m=model_fun(**kwargs)
            m=initialize_model(m,from_feasible=True,feasible_model=init_name) 
            textbuffer = io.StringIO()
            for v in m.component_objects(pe.Var, descend_into=True):
                v.pprint(textbuffer)
                textbuffer.write('\n')
            textbuffer.write('\n Objective: \n') 
            textbuffer.write(str(pe.value(m.obj)))  
            file_name=init_name+'.txt'  
            with open(os.path.join('C:/Users/dlinanro/Desktop/GeneralBenders/scheduling_and_control',file_name), 'w') as outputfile:
                outputfile.write(textbuffer.getvalue())
            # plot of states
            for k in m.K:
                t_pro=[]
                state=[]
                for t in m.T:
                    t_pro.append(m.t_p[t]*60*60)
                    state.append(pe.value(m.S[k,t]))

                plt.plot(t_pro, state,color='red')
                plt.xlabel('Time [s]')
                plt.ylabel('State level $[m^{3}]$')
                title='state '+k
                plt.title(title)
                # plt.show()
                plt.savefig("figures/case_2_example/"+title+".svg") 
                plt.clf()
                plt.cla()
                plt.close()

            #--------------------------------- Gantt plot--------------------------------------------
            fig, gnt = plt.subplots(figsize=(11, 5), sharex=True, sharey=False)
            # Setting Y-axis limits
            gnt.set_ylim(8, 52) #TODO: change depending case study
            
            # Setting X-axis limits
            gnt.set_xlim(0, m.lastT.value*m.delta.value*60*60)
            
            # Setting labels for x-axis and y-axis
            gnt.set_xlabel('Time [s]')
            gnt.set_ylabel('Units')
            
            # Setting ticks on y-axis
            gnt.set_yticks([15, 25, 35, 45]) #TODO: change depending case study
            # Labelling tickes of y-axis
            gnt.set_yticklabels(['U4', 'U3', 'U2', 'U1']) #TODO: change depending case study
            
            
            # Setting graph attribute
            gnt.grid(False)
            
            # Declaring bars in schedule
            height=9
            already_used=[]
            for j in m.J:

                if j=='U1':
                    lower_y_position=40    
                elif j=='U2':
                    lower_y_position=30    
                elif j=='U3':
                    lower_y_position=20
                elif j=='U4':
                    lower_y_position=10
                for i in m.I:
                    if i=='T1':
                        bar_color='tab:red'
                    elif i=='T2':
                        bar_color='tab:green'    
                    elif i=='T3':
                        bar_color='tab:blue'    
                    elif i=='T4':
                        bar_color='tab:orange' 
                    elif i=='T5':
                        bar_color='tab:olive'
                    for t in m.T:
                        try:
                            if i in m.I_dynamics and j in m.J_dynamics:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                                              
                            elif i in m.I_distil and j in m.J_distil:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                                                                          
                            else:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                        

                        except:
                            pass 
            gnt.tick_params(axis='both', which='major', labelsize=15)
            gnt.tick_params(axis='both', which='minor', labelsize=15) 
            gnt.yaxis.label.set_size(15)
            gnt.xaxis.label.set_size(15)
            plt.legend()
            # plt.show()
            plt.savefig("figures/case_2_example/gantt_minlp.svg")   
            plt.clf()
            plt.cla()
            plt.close()


        else:

            # plot of states
            for k in m.K:
                t_pro=[]
                state=[]
                for t in m.T:
                    t_pro.append(m.t_p[t]*60*60)
                    state.append(pe.value(m.S[k,t]))

                plt.plot(t_pro, state,color='red')
                plt.xlabel('Time [s]')
                plt.ylabel('State level $[m^{3}]$')
                title='state '+k
                plt.title(title)
                # plt.show()
                plt.savefig("figures/case_2_example/"+title+".svg") 
                plt.clf()
                plt.cla()
                plt.close()

            #--------------------------------- Gantt plot--------------------------------------------
            fig, gnt = plt.subplots(figsize=(11, 5), sharex=True, sharey=False)
            # Setting Y-axis limits
            gnt.set_ylim(8, 52) #TODO: change depending case study
            
            # Setting X-axis limits
            gnt.set_xlim(0, m.lastT.value*m.delta.value*60*60)
            
            # Setting labels for x-axis and y-axis
            gnt.set_xlabel('Time [s]')
            gnt.set_ylabel('Units')
            
            # Setting ticks on y-axis
            gnt.set_yticks([15, 25, 35, 45]) #TODO: change depending case study
            # Labelling tickes of y-axis
            gnt.set_yticklabels(['U4', 'U3', 'U2', 'U1']) #TODO: change depending case study
            
            
            # Setting graph attribute
            gnt.grid(False)
            
            # Declaring bars in schedule
            height=9
            already_used=[]
            for j in m.J:

                if j=='U1':
                    lower_y_position=40    
                elif j=='U2':
                    lower_y_position=30    
                elif j=='U3':
                    lower_y_position=20
                elif j=='U4':
                    lower_y_position=10
                for i in m.I:
                    if i=='T1':
                        bar_color='tab:red'
                    elif i=='T2':
                        bar_color='tab:green'    
                    elif i=='T3':
                        bar_color='tab:blue'    
                    elif i=='T4':
                        bar_color='tab:orange' 
                    elif i=='T5':
                        bar_color='tab:olive'
                    for t in m.T:
                        try:
                            if i in m.I_dynamics and j in m.J_dynamics:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                                              
                            elif i in m.I_distil and j in m.J_distil:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, m.varTime[i,j,t].value*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+m.varTime[i,j,t].value*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                                                                          
                            else:
                                if round(pe.value(m.X[i,j,t]))==1 and all(i!=already_used[kkk] for kkk in range(len(already_used))):
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black",label=i)
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')
                                    already_used.append(i)
                                elif round(pe.value(m.X[i,j,t]))==1:
                                    gnt.broken_barh([(m.t_p[t]*60*60, pe.value(m.tau_p[i,j])*60*60)], (lower_y_position, height),facecolors =bar_color,edgecolor="black")
                                    gnt.annotate("{:.2f}".format(m.B[i,j,t].value),xy=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),xytext=((2*m.t_p[t]*60*60+pe.value(m.tau_p[i,j])*60*60)/2,(2*lower_y_position+height)/2),fontsize = 15,horizontalalignment='center')                        

                        except:
                            pass 
            gnt.tick_params(axis='both', which='major', labelsize=15)
            gnt.tick_params(axis='both', which='minor', labelsize=15) 
            gnt.yaxis.label.set_size(15)
            gnt.xaxis.label.set_size(15)
            plt.legend()
            # plt.show()
            plt.savefig("figures/case_2_example/gantt_minlp.svg")   
            plt.clf()
            plt.cla()
            plt.close()
