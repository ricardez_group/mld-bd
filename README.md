# Multicut logic-based Benders decomposition for discretetime scheduling and dynamic optimization of network batch plants 

This repository contains the MLD-BD algorithm code for simultaneous scheduling and dynamic optimization including:

- Codes with Pyomo models in "distillation_model", "case_study_1_model.py" and "case_study_2_model.py" 
- Codes required to run computational experiments in "MLDBD_experiments.py" and "GBD_experiments.py"
- Codes required to generate plots in "case_study_1_plots.py" and "case_study_2_plots.py"


This code was written using the Pyomo modeling environment (http://www.pyomo.org/). The functions used to solve and reinitialize subproblems include code snippets from (https://github.com/SECQUOIA/dsda-gdp) and the model_serializer.py code from (https://github.com/IDAES/idaes-pse). The MINLP optimization problems involved were solved using solvers available through GAMS (https://www.gams.com/).

- Python version: cpython 3.10.11
- Pyomo version: 6.4.4
- GAMS version: 41.5.0
- Computer characteristics: Windows 10, Intel® Core™ i7-9700K CPU @ 3.60 GHz with 64 GB of RAM














